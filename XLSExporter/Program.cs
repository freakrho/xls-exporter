﻿using System;
using System.IO;
using System.Linq;
using OfficeOpenXml;

namespace XLSExporter {
    class Program {
        static readonly string[] EXTENSIONS = { ".xls", ".xlsx" };

        static bool IsExcel( string path ) {
            return EXTENSIONS.Contains(Path.GetExtension(path).ToLower());
        }

        static void Main( string[] args ) {
            if ( args.Length > 0 ) {
                var filePath = args[0];
                if ( !IsExcel(filePath) ) {
                    Console.WriteLine($"File {filePath} is not an Excel file");
                    return;
                }

                var output = Path.GetDirectoryName(filePath);
                if ( args.Length > 1 ) {
                    output = args[1];
                    if ( !Directory.Exists(output) ) {
                        Directory.CreateDirectory(output);
                    }
                }
                var extension = "csv";
                if ( args.Length > 2 ) {
                    extension = args[2];
                }

                FileInfo excelFile = new FileInfo(filePath);
                ExcelPackage pck = new ExcelPackage(excelFile);
                for ( int i = 0; i < pck.Workbook.Worksheets.Count; i++ ) {
                    var sheet = pck.Workbook.Worksheets[i];
                    if ( sheet.Name.StartsWith("!") ) continue;

                    var bytes = EpplusCsvConverter.ConvertToCsv(pck, i);
                    File.WriteAllBytes(Path.Combine(output, $"{sheet.Name}.{extension}"), bytes);
                }
            }
        }
    }
}
